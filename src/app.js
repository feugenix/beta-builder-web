'use strict';

const PORT = 3333;

import express from 'express';
import * as path from 'path';

let app = express();

app.use(express.static('static'));

app.listen(PORT, () => console.log(`Running on ${PORT} port`));