'use strict';

import Data from 'blocks/data/data';
import BL from 'blocks/bl/bl';
import View from 'blocks/view/view';

new View(new BL(new Data()));