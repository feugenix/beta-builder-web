'use strict';

import Hogan from 'hogan';
import PubSub from 'pubsub';

export default class BetasManager extends PubSub {
    constructor(container, template) {
        super();

        if (!container) {
            console.log('No betas container');
            return;
        }

        if (!template) {
            console.log('No beta template');
            return;
        }

        this._betasContainer = container;
        this._betasContainer.innerHTML = '';

        this._betaTemplate = Hogan.compile(template);
    }

    getBetaNodeById(betaId) {
        return this._betasContainer.querySelector(`#uid${betaId}`);
    }

    add(betaInfo) {
        let fakeDiv = document.createElement('div');
        fakeDiv.innerHTML = this._betaTemplate.render({betas: [betaInfo]});
        for (let child of fakeDiv.childNodes) {
            this._betasContainer.appendChild(child.cloneNode(true));
        }
        fakeDiv.remove();

        let newBeta = this.getBetaNodeById(betaInfo.uid);

        if (!newBeta) {
            return;
        }

        newBeta.querySelector('.beta-controls__remove').addEventListener('click', e => {
            e.preventDefault();

            this.remove(betaInfo);

            this.publish('remove beta', betaInfo.uid);
        });
    }

    onProgress(msg, betaInfo) {
        // TODO: Show build progress.
    }

    onQueued(msg, betaInfo) {
        let beta = this.getBetaNodeById(betaInfo.uid);

        if (!beta) {
            this.add(betaInfo);
        }

        beta = this.getBetaNodeById(betaInfo.uid);

        beta.querySelector('.beta__progress').classList.add('beta__progress_visible');
    }

    onFail(msg, betaInfo) {
        let beta = this.getBetaNodeById(betaInfo.uid);

        beta.querySelector('.beta__progress').classList.remove('beta__progress_visible');

        beta.classList.add('beta_build_fail');
    }

    onSuccess(msg, betaInfo) {
        let beta = this.getBetaNodeById(betaInfo.uid);

        beta.querySelector('.beta__progress').classList.remove('beta__progress_visible');

        beta.classList.add('beta_build_success');
    }

    remove(betaInfo) {
        let betaNode = this.getBetaNodeById(betaInfo.uid);

        if (!betaNode) {
            return;
        }

        betaNode.remove();
    }
};