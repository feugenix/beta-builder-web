'use strict';

import PubSub from 'pubsub';

const WEB_SOCKET_OPEN = 1,
      PORT = 3000,
      SOCKET_URL = `ws://localhost:${PORT}`;

let socket = new WebSocket(SOCKET_URL);

export default class Data extends PubSub {
    constructor() {
        super();

        socket.onerror = e => {
            this.publish('error', e);
        };

        socket.onclose = e => {
            this.publish('close', e);
        };

        socket.onopen = e => {
            this.publish('open', e);
        };

        socket.onmessage = e => {
            this.publish('message', e.data);
        };
    }

    send(req) {
        socket.readyState === WEB_SOCKET_OPEN && socket.send(JSON.stringify(req));
    }
};