'use strict';

import PubSub from 'pubsub';

export default class BL extends PubSub {
    constructor(dataLayer) {
        super();

        if (!dataLayer) {
            console.log('No DataLayer!');
        }

        this._dl = dataLayer;

        this._dl.subscribe('error', e => {
            console.log(e);
        });

        this._dl.subscribe('open', e => {
            let req = {
                type: 'list betas'
            };

            this._dl.send(req);
        });

        this._dl.subscribe('close', e => {
            console.log('socket is closed');
        });

        this._dl.subscribe('message', data => {
            let msg = JSON.parse(data);

            switch (msg.type) {
                case 'list betas':
                    this.publish('list betas', msg.data);
                break;

                case 'fail':
                    this.publish('fail', msg.msg, msg.data);
                break;

                case 'success':
                    switch (msg.msg) {
                        case 'creating':
                            this.publish('success', 'creating', msg.data);
                        break;

                        case 'restoring':
                            this.publish('success', 'restoring', msg.data);
                        break;

                        default: console.log(`Unsupported type of success event: ${msg.msg}`);
                    }
                break;

                case 'removed':
                    this.publish('removed', msg.data);
                break;

                case 'progress':
                    this.publish('progress', msg.msg, msg.data);
                break;

                case 'queued':
                    this.publish('queued', msg.msg, msg.data);
                break;

                case 'notification':
                    // this.publish('notification', msg.data);
                    // TODO: Rethink, do we need this?
                break;

                default:
                    console.log(`Unsupported event ${msg.type}`);
            }
        });
    }

    event(data) {
        this._dl.send(data);
    }
};