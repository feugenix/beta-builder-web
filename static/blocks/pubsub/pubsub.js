'use strict';

export default class PubSub {
    constructor() {
        this.handlers = new Map();
    }

    subscribe(event, cb) {
        if (!this.handlers.has(event)) {
            this.handlers.set(event, new Set());
        }
        this.handlers.get(event).add(cb);
    }

    unsubscribe(event, cb) {
        if (!this.handlers.has(event)) {
            return;
        }

        let handlers = this.handlers.get(event);

        handlers.delete(cb);

        if (handlers.size === 0) {
            this.handlers.delete(event);
        }
    }

    publish(event, ...data) {
        if (!this.handlers.has(event)) {
            return;
        }

        let handlers = this.handlers.get(event);

        for (let cb of handlers) {
            setTimeout(() => cb(...data), 0);
        }
    }
};