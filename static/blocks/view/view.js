'use strict';

import Hogan from 'hogan';
import PubSub from 'pubsub';
import BetasManager from 'blocks/betas_manager/betas_manager';
import Popup from 'blocks/popup/popup';

let $$ = selector => document.querySelector(selector);

export default class View extends PubSub {
    constructor(businessLogic) {
        super();

        if (!businessLogic) {
            console.log('WTF, no Business Logic!');
            return;
        }

        this._bl = businessLogic;
        this._betasManager = new BetasManager($$('.betas-container'), $$('#beta-template').innerHTML);
        this._createNewBetaPopup = new Popup($$('.create-beta-popup'));

        this.bindEvents();
    }

    bindEvents() {
        $$('.create-beta__control').addEventListener('click', e => {
            e.preventDefault();
            this._createNewBetaPopup.show();
        });

        this._bl.subscribe('list betas', betas => betas.forEach(beta => this._betasManager.add(beta)));

        this._bl.subscribe('success', (msg, beta) => this._betasManager.onSuccess(msg, beta));

        this._bl.subscribe('removed', beta => this._betasManager.remove(beta));

        this._bl.subscribe('progress', (msg, beta) => this._betasManager.onProgress(msg, beta));

        this._bl.subscribe('queued', (msg, beta) => this._betasManager.onQueued(msg, beta));

        this._betasManager.subscribe('remove beta', betaUid => {
            this._sendEvent('remove beta', betaUid);
        });

        this._createNewBetaPopup.subscribe('data', ({title, gitUrl/*, settingsLocal */}) => {
            this._sendEvent('create beta', {title, gitUrl});
        });
    }

    _sendEvent(type, data) {
        this._bl.event({type, data});
    }
};
