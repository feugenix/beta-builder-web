'use strict';

import Hogan from 'hogan';
import PubSub from 'pubsub';

export default class Popup extends PubSub {
    constructor(popupNode) {
        super();

        if (!popupNode) {
            console.log('No popup dom node');
            return;
        }

        this._domElem = popupNode;
        this._isShown = false;

        this._titleInput = this._domElem.querySelector('.create-beta-popup__title-input');
        this._branchInput = this._domElem.querySelector('.create-beta-popup__branch');

        this.bindEvents();
    }

    bindEvents() {
        let hidePopup = e => {
            e.preventDefault();
            this.hide();
        };

        this._domElem
            .querySelector('.create-beta-popup__ok')
            .addEventListener('click', this.publishData.bind(this));

        this._domElem
            .querySelectorAll('.create-beta-popup__ok, .create-beta-popup__cancel, .create-beta-popup__close, .create-beta-popup__under')
            .forEach(node => node.addEventListener('click', hidePopup));

        document.body.addEventListener('keydown', e => {
            if (!this._isShown) {
                return;
            }

            if (e.key && e.key.toLowerCase === 'escape' || e.keyCode === 27) {
                this.hide();
            }

            if (e.key && e.key.toLowerCase === 'enter' || e.keyCode === 13) {
                this.publishData();
                this.hide();
            }
        });
    }

    publishData() {
        let title = this._titleInput.value,
            gitUrl = this._branchInput.value;

        this.publish('data', {title, gitUrl});
    }

    show() {
        this._isShown = true;
        this._titleInput.value = '';
        this._branchInput.value = '';
        this._domElem.classList.add('create-beta-popup_visible');
    }

    hide() {
        this._isShown = false;
        this._domElem.classList.remove('create-beta-popup_visible');
    }
};